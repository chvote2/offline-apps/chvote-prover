/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-prover-1.0.0                                                                            -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2019 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package com.demtechgroup.chvote;

import com.demtechgroup.chvote.chport.service.model.DecryptionProof;
import com.demtechgroup.chvote.chport.service.model.Encryption;
import com.demtechgroup.chvote.chport.service.model.EncryptionGroup;
import com.demtechgroup.chvote.chport.service.model.PublicParameters;
import com.demtechgroup.chvote.chport.service.support.JacobiSymbol;
import java.math.BigInteger;
import java.util.List;

/**
 * General class for performing parameter and field validations in accordance with the
 * "CHVote System Specification" Version 1.2.
 */
public class ArgumentValidator {
  private static final JacobiSymbol jacobiSymbol = new JacobiSymbol();
  private final EncryptionGroup  encryptionGroup;
  private       PublicParameters publicParameters;

  /**
   * Base constructor.
   *
   * @param publicParameters the public parameters values are to be validated against.
   */
  public ArgumentValidator(PublicParameters publicParameters) {
    this.publicParameters = publicParameters;
    this.encryptionGroup = publicParameters.getEncryptionGroup();
  }

  /**
   * Return the public parameters used to create this validator.
   *
   * @return the public parameters for this validator.
   */
  public PublicParameters getPublicParameters() {
    return publicParameters;
  }

  /**
   * Test the passed in condition, throwing an IllegalArgumentException if it is false.
   *
   * @param condition the condition being asserted.
   * @param label     label for the message to throw in case the assertion is incorrect.
   */
  public void assertTrue(boolean condition, String label) {
    if (!condition) {
      throw new IllegalArgumentException(MessageUtils.getMessage(label));
    }
  }

  /**
   * Verify the passed in value meets the criteria of x &isin; encryptionGroup.
   *
   * @param x     the value to check.
   * @param label label for message to throw in case the assertion is incorrect.
   */
  public void isMember(BigInteger x, String label) {
    if (!isMember(x)) {
      throw new IllegalArgumentException(MessageUtils.getMessage(label));
    }
  }

  /**
   * Verify the passed in list of values contains only fields that
   * meet the criteria of x &isin; encryptionGroup.
   *
   * @param values list of values to check.
   * @param label  label for message to throw in case the assertion is incorrect.
   */
  @SuppressWarnings("unchecked")
  public void containsOnlyMembers(List values, String label) {
    if (values.get(0) instanceof Encryption) {
      for (Encryption e : (List<Encryption>) values) {
        if (!isMember(e.getA()) || !isMember(e.getB())) {
          throw new IllegalArgumentException(MessageUtils.getMessage(label));
        }
      }
    } else if (values.get(0) instanceof BigInteger) {
      for (BigInteger i : (List<BigInteger>) values) {
        if (!isMember(i)) {
          throw new IllegalArgumentException(MessageUtils.getMessage(label));
        }
      }
    } else if (values.get(0) instanceof List) {
      for (List l : (List<List>) values) {
        containsOnlyMembers(l, label);
      }
    } else {
      throw new IllegalArgumentException("cannot verify argument");
    }
  }

  /**
   * Verify the passed in value meets the criteria of x &isin; Z_q.
   *
   * @param x     the value to check.
   * @param label label for message to throw in case the assertion is incorrect.
   */
  public void isInZ_q(BigInteger x, String label) {
    if (!isInZ_q(x)) {
      throw new IllegalArgumentException(MessageUtils.getMessage(label));
    }
  }

  /**
   * Verify the passed in list contains only field values meeting
   * the criteria of x &isin; Z_q.
   *
   * @param values list of values to check.
   * @param label  label for message to throw in case the assertion is incorrect.
   */
  public void containsOnlyInZ_q(List<BigInteger> values, String label) {
    if (values.get(0) != null) {
      for (BigInteger i : values) {
        if (!isInZ_q(i)) {
          throw new IllegalArgumentException(MessageUtils.getMessage(label));
        }
      }
    } else {
      throw new IllegalArgumentException(MessageUtils.getMessage("Validator.unknown_list_error"));
    }
  }

  /**
   * Verify that for all of the passed in DecryptionProofs that the T vector contains only
   * member values, and the S is &isin; Z_q.
   *
   * @param decryptionProofs the list of proofs to check.
   * @param label            label for message to throw in case the assertion is incorrect.
   */
  public void checkDecryptionProofValues(List<DecryptionProof> decryptionProofs, String label) {
    for (DecryptionProof p : decryptionProofs) {
      containsOnlyMembers(p.getT(), label);
      if (!isInZ_q(p.getS())) {
        throw new IllegalArgumentException(MessageUtils.getMessage(label));
      }
    }
  }

  // base checks

  /**
   * Algorithm 7.2 : isMember
   *
   * @param x A number
   *
   * @return true if x &isin; encryptionGroup, false otherwise
   */
  private boolean isMember(BigInteger x) {
    int j = jacobiSymbol.computeJacobiSymbol(x, encryptionGroup.getP());
    return x.compareTo(BigInteger.ONE) >= 0 && x.compareTo(encryptionGroup.getP()) < 0 && j == 1;
  }

  /**
   * Utility to verify membership for Z_q
   *
   * @param x a number
   *
   * @return true if x &isin; Z_q, false otherwise
   */
  private boolean isInZ_q(BigInteger x) {
    return x.compareTo(BigInteger.ZERO) >= 0 && x.compareTo(encryptionGroup.getQ()) < 0;
  }
}
