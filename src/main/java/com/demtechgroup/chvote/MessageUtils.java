/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-prover-1.0.0                                                                            -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2019 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package com.demtechgroup.chvote;

import java.io.UnsupportedEncodingException;
import java.text.MessageFormat;
import java.util.Locale;
import java.util.ResourceBundle;

/**
 * Utility class for handling internationalised messages.
 */
public class MessageUtils {
  private MessageUtils() {
  }

  private static final String RESOURCE_NAME = "com.demtechgroup.chvote.messages";

  private static Locale locale = Locale.getDefault();

  public static void setLocale(Locale loc) {
    locale = loc;
  }

  public static String getMessage(String label) {
    ResourceBundle bundle;
    ClassLoader loader;

    loader = Verifier.class.getClassLoader();

    if (loader == null) {
      bundle = ResourceBundle.getBundle(RESOURCE_NAME, locale);
    } else {
      bundle = ResourceBundle.getBundle(RESOURCE_NAME, locale, loader);
    }

    return getString(bundle, label);
  }

  public static String getMessage(String label, int value) {
    Object[] args = {value};

    return buildMessage(label, args);
  }


  public static String getMessage(String label, int value1, int value2) {
    Object[] args = {value1, value2};

    return buildMessage(label, args);
  }

  public static String getMessage(String label, String value) {
    Object[] args = {value};

    return buildMessage(label, args);
  }

  private static String buildMessage(String label, Object[] args) {
    ClassLoader loader;
    ResourceBundle bundle;
    loader = Verifier.class.getClassLoader();

    if (loader == null) {
      bundle = ResourceBundle.getBundle(RESOURCE_NAME, locale);
    } else {
      bundle = ResourceBundle.getBundle(RESOURCE_NAME, locale, loader);
    }

    MessageFormat formatter = new MessageFormat("");
    formatter.setLocale(Locale.getDefault());

    formatter.applyPattern(getString(bundle, label));

    return formatter.format(args);
  }

  private static String getString(ResourceBundle bundle, String label) {
    try {
      return new String(bundle.getString(label).getBytes("ISO-8859-1"), "UTF-8");
    } catch (UnsupportedEncodingException e) {
      return bundle.getString(label);  // hope for the best
    }
  }
}
