/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-prover-1.0.0                                                                            -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2019 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package com.demtechgroup.chvote;

/**
 * Carrier class for a proof result message and data.
 */
public class ProofResult
    extends ProofMessage {
  private final boolean okay;
  private final String  message;
  private final byte[]  hash;

  /**
   * Construct a success message.
   *
   * @param message details for the proof solved.
   * @param hash    the hash of the challenges computed over the course of confirming the proof.
   */
  public ProofResult(String message, byte[] hash) {
    this(true, message, hash);
  }

  /**
   * Construct a failure message.
   *
   * @param error the error message to use.
   */
  public ProofResult(String error) {
    this(false, error, null);
  }

  private ProofResult(boolean okay, String message, byte[] hash) {
    super(message);

    this.okay = okay;
    this.message = message;
    this.hash = (hash != null) ? hash.clone() : null;
  }

  /**
   * Return true if this proof result represents a proof that worked.
   *
   * @return true if proof passed, false otherwise.
   */
  public boolean isOkay() {
    return okay;
  }

  /**
   * Return a formatted string detailing the result.
   *
   * @return the resul as a String.
   */
  public String toString() {
    StringBuilder sb = new StringBuilder(message);

    sb.append(": ");
    if (okay) {
      sb.append(MessageUtils.getMessage("ProofResult.passed"));
    } else {
      sb.append(MessageUtils.getMessage("ProofResult.failed"));
    }
    if (okay && hash != null) {
      sb.append(" - ");
      sb.append(MessageUtils.getMessage("ProofResult.hash_name"));
      sb.append("(\"");
      sb.append(toHexString(hash));
      sb.append("\")");
    }

    return sb.toString();
  }

  private static final char[] encodingTable =
      {
          (byte) '0', (byte) '1', (byte) '2', (byte) '3', (byte) '4', (byte) '5', (byte) '6', (byte) '7',
          (byte) '8', (byte) '9', (byte) 'a', (byte) 'b', (byte) 'c', (byte) 'd', (byte) 'e', (byte) 'f'
      };

  private static String toHexString(byte[] bytes) {
    StringBuilder sb = new StringBuilder(bytes.length * 2);
    for (int i = 0; i != bytes.length; i++) {
      int b = bytes[i] & 0xff;

      sb.append(encodingTable[b >> 4]);
      sb.append(encodingTable[b & 0xf]);
    }

    return sb.toString();
  }

  public byte[] getHash() {
    return (hash != null) ? hash.clone() : null;
  }
}
