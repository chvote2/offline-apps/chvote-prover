/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-prover-1.0.0                                                                            -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2019 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package com.demtechgroup.chvote;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Utility class for producing time stamps.
 */
class Timestamp {

  private Timestamp() {
  }

  /**
   * Return a string representation of the current time in a format like:  "2017-10-05 22:14:48+1100"
   *
   * @return the current time a String time stamp.
   */
  public static String getTimestamp() {
    SimpleDateFormat fmt = new SimpleDateFormat("[yyyy-MM-dd HH:mm:ssZ]");

    return fmt.format(new Date());
  }
}
