/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-prover-1.0.0                                                                            -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2019 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package com.demtechgroup.chvote.bc;

import com.demtechgroup.chvote.HashCalculator;
import com.demtechgroup.chvote.HashFactory;
import com.demtechgroup.chvote.MessageUtils;
import com.demtechgroup.chvote.chport.service.model.SecurityParameters;

/**
 * Factory class for manufacturing HashCalculators.
 */
public class Blake2BHashFactory
    implements HashFactory {
  private final SecurityParameters securityParameters;
  private final int                bitStrength;

  /**
   * BaseConstructor.
   *
   * @param bitStrength        bit size of digest to use.
   * @param securityParameters the security parameters to vet the digest requested against.
   */
  public Blake2BHashFactory(int bitStrength, SecurityParameters securityParameters) {
    if ((bitStrength / 8) < securityParameters.getUpper_l()) {
      throw new IllegalArgumentException(
          MessageUtils.getMessage("HashFactory.length_error", (bitStrength / 8), securityParameters.getUpper_l()));
    }
    this.bitStrength = bitStrength;
    this.securityParameters = securityParameters;
  }

  /**
   * Create a new calculator.
   *
   * @return a new HashCalculator.
   */
  @Override
  public final HashCalculator createCalculator() {
    return new Blake2BHashCalculator(bitStrength, securityParameters.getUpper_l());
  }
}
