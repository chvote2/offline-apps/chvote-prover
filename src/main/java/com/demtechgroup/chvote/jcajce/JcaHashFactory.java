/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-prover-1.0.0                                                                            -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2019 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package com.demtechgroup.chvote.jcajce;

import com.demtechgroup.chvote.HashFactory;
import com.demtechgroup.chvote.MessageUtils;
import com.demtechgroup.chvote.chport.service.model.SecurityParameters;
import java.security.GeneralSecurityException;
import java.security.MessageDigest;

/**
 * Factory class for manufacturing HashCalculators.
 */
public class JcaHashFactory
    implements HashFactory {
  private final String             algorithm;
  private final String             provider;
  private final SecurityParameters securityParameters;

  /**
   * BaseConstructor.
   *
   * @param algorithm          the underlying digest algorithm to use in calculators made by this factory.
   * @param provider           the provider to use as the source of digest implementations.
   * @param securityParameters the security parameters to vet the digest requested against.
   */
  public JcaHashFactory(String algorithm, String provider, SecurityParameters securityParameters) {
    this.algorithm = algorithm;
    this.provider = provider;
    this.securityParameters = securityParameters;
    JcaHashCalculator hash = createCalculator();
    if (hash.getDigestLength() < securityParameters.getUpper_l()) {
      throw new IllegalArgumentException(
          MessageUtils.getMessage("HashFactory.length_error", hash.getDigestLength(), securityParameters.getUpper_l()));
    }
  }

  /**
   * Create a new calculator.
   *
   * @return a new HashCalculator.
   */
  @Override
  public final JcaHashCalculator createCalculator() {
    try {
      MessageDigest digest = MessageDigest.getInstance(algorithm, provider);

      return new JcaHashCalculator(digest, securityParameters.getUpper_l());
    } catch (GeneralSecurityException e) {
      throw new IllegalStateException(MessageUtils.getMessage("HashFactory.cannot_create_error") + e.getMessage(), e);
    }
  }
}
