/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-prover-1.0.0                                                                            -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2019 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package com.demtechgroup.chvote.json;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.Reader;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

/**
 * A basic JSON parser - builds a tree of JSONNode objects from the input file.
 */
public class JSONParser {
  private final CountingReader reader;

  private boolean firstObject;

  public JSONParser(Reader reader) {
    this.reader = new CountingReader(reader);

    this.firstObject = true;
  }

  /**
   * Read the next node and all of its children.
   *
   * @return the next node in the input.
   *
   * @throws IOException on a parsing exception.
   */
  public JSONNode readNode()
      throws IOException {
    if (firstObject) {
      // skip till opening brace
      isCharacter('{');
      firstObject = false;
    } else {
      isCharacter(',');
    }

    return object();
  }

  private JSONNode object()
      throws IOException {
    int c = skipWhiteSpace();

    if (c < 0) // EOF
    {
      return null;
    }

    if (c == '}') // the final closing brace
    {
      isCharacter('}');

      return null;
    }

    isCharacter('"');

    String name = name();

    isCharacter(':');
    isCharacter('{');
    JSONNode contents = body(name);
    isCharacter('}');

    return contents;
  }

  private String name()
      throws IOException {
    StringBuilder sb = new StringBuilder();
    int c;
    while ((c = reader.read()) >= 0 && c != '"') {
      sb.append((char) c);
    }

    return sb.toString();
  }

  private boolean isCharacter(char c)
      throws IOException {
    skipWhiteSpace();

    reader.mark(1);
    if (reader.read() != c) {
      reader.reset();
      return false;
    }

    return true;
  }

  private JSONNode body(String nodeName)
      throws IOException {
    List<JSONNode> contents = new ArrayList<JSONNode>();
    JSONNode value;

    skipWhiteSpace();

    if (isCharacter('[')) {
      value = array(nodeName);

      isCharacter(']');

      return value;
    }

    do {
      isCharacter('"');

      String name = name();

      isCharacter(':');

      if (isCharacter('{')) {
        value = body(name);

        isCharacter('}');
      } else if (isCharacter('[')) {
        value = array(name);

        isCharacter(']');
      } else if (isCharacter('"')) {
        value = new JSONNode(JSONNode.Type.STRING, name, name());
      } else if (isCharacter('n')) {
        value = new JSONNode(JSONNode.Type.NULL, name, readNull());
      } else {
        value = readNumber(name);
      }

      contents.add(value);
    }
    while (isCharacter(','));

    return new JSONNode(JSONNode.Type.BODY, nodeName, contents);
  }

  private Object readNull()
      throws IOException {
    isCharacter('u');
    isCharacter('l');
    isCharacter('l');

    return null;
  }

  private JSONNode array(String name)
      throws IOException {
    List<JSONNode> contents = new ArrayList<JSONNode>();

    do {
      if (isCharacter('"')) {
        contents.add(new JSONNode(JSONNode.Type.STRING, name()));
      } else if (isCharacter('{')) {
        contents.add(body(null));

        isCharacter('}');
      } else if (isCharacter('[')) {
        contents.add(array(null));

        isCharacter(']');
      } else {
        contents.add(readNumber(null));
      }
    }
    while (isCharacter(','));

    return new JSONNode(JSONNode.Type.ARRAY, name, contents);
  }

  private JSONNode readNumber(String name)
      throws IOException {
    int c;
    boolean dotDetected = false;

    StringBuilder sb = new StringBuilder();

    reader.mark(1);
    while ((c = reader.read()) >= 0 && (Character.isDigit(c) || c == '.' || c == '-')) {
      sb.append((char) c);
      if (c == '.') {
        dotDetected = true;
      }
      reader.mark(1);
    }

    reader.reset();

    try {
      if (dotDetected) {
        return new JSONNode(JSONNode.Type.DECIMAL, name, new BigDecimal(sb.toString()));
      } else {
        return new JSONNode(JSONNode.Type.INTEGER, name, new BigInteger(sb.toString()));
      }
    } catch (NumberFormatException e) {
      throw new IOException(e.getMessage() + ": " + reader.getLineCount(), e);
    }
  }

  private int skipWhiteSpace()
      throws IOException {
    int c;
    reader.mark(1);
    while ((c = reader.read()) >= 0 && (c == ' ' || c == '\r' || c == '\t' || c == '\n')) {
      reader.mark(1);
    }
    reader.reset();

    return c;
  }

  /**
   * A BufferedReader that keeps track of line numbers.
   */
  private static class CountingReader
      extends BufferedReader {
    private int lineCount = 1;

    public CountingReader(Reader in) {
      super(in, 128 * 1024);
    }

    public int read()
        throws IOException {
      int c = super.read();
      if (c == '\n') {
        lineCount++;
      }

      return c;
    }

    public int getLineCount() {
      return lineCount;
    }
  }
}
